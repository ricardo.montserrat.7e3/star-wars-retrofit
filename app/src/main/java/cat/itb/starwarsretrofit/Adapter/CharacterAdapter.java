package cat.itb.starwarsretrofit.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.starwarsretrofit.Objects.Character;
import cat.itb.starwarsretrofit.R;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>
{
    private List<Character> characters;
    private OnItemClickListenerAdapter onItemClickListenerAdapter;
    private ArrayList<Character> charactersArray = new ArrayList<>();

    public CharacterAdapter(List<Character> characters, OnItemClickListenerAdapter onItemClickListenerAdapter)
    {
        this.characters = characters;
        this.onItemClickListenerAdapter = onItemClickListenerAdapter;
        charactersArray.addAll(characters);
    }

    public void setCharacters(List<Character> charactersList)
    {
        this.characters.clear();
        this.characters.addAll(charactersList);
        charactersArray.clear();
        charactersArray.addAll(characters);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new CharacterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.character_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position)
    {
        Character currentCharacter = characters.get(position);
        holder.birthYearTextView.setText(currentCharacter.getBirthYear());
        holder.heightTextView.setText(currentCharacter.getHeight());
        holder.hairColorTextView.setText(currentCharacter.getHairColor());
        holder.nameTextView.setText(currentCharacter.getName());
        holder.itemView.setOnClickListener(v -> onItemClickListenerAdapter.OnItemClick(currentCharacter));
    }

    @Override
    public int getItemCount() { return characters.size(); }

    public static class CharacterViewHolder extends RecyclerView.ViewHolder
    {
        TextView nameTextView, hairColorTextView, heightTextView, birthYearTextView;

        public CharacterViewHolder(@NonNull View itemView)
        {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.text_view_name_value);
            hairColorTextView = itemView.findViewById(R.id.text_view_hair_color_value);
            heightTextView = itemView.findViewById(R.id.text_view_height_value);
            birthYearTextView = itemView.findViewById(R.id.text_view_birth_value);
        }
    }

    public interface OnItemClickListenerAdapter
    {
        void OnItemClick(Character character);
    }

    public boolean filter(String text)
    {
        if(text.isEmpty()) return false;
        text = text.toLowerCase();
        characters.clear();
        for (Character character : charactersArray) { if(character.getName().toLowerCase().contains(text)) characters.add(character); }
        notifyDataSetChanged();
        return characters.size() > 0;
    }
}
