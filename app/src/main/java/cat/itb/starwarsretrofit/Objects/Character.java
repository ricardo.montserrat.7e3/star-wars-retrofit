package cat.itb.starwarsretrofit.Objects;

import com.google.gson.annotations.SerializedName;

public class Character
{
    private String name, height;
    @SerializedName("hair_color")
    private String hairColor;
    @SerializedName("birth_year")
    private String birthYear;

    private String[] films;

    public String getName() { return name; }

    public String getHeight() { return height; }

    public String getHairColor() { return hairColor; }

    public String getBirthYear() { return birthYear; }

    public String[] getFilms() { return films; }
}
