package cat.itb.starwarsretrofit.Objects;

import java.util.List;

public class Data
{
    private List<Character> results;
    private String next, previous;

    public List<Character> getResults()
    {
        return results;
    }

    public String getNext()
    {
        if (next == null) return "";
        String[] nextSplit = next.split("/");
        return "people/" + nextSplit[nextSplit.length - 1];
    }

    public String getPrevious()
    {
        if (previous == null) return "";
        String[] previousSplit = previous.split("/");
        return "people/" + previousSplit[previousSplit.length - 1];
    }
}
