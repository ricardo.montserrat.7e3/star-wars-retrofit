package cat.itb.starwarsretrofit.Objects;

import com.google.gson.annotations.SerializedName;

public class Film
{
    private String title;
    @SerializedName("opening_crawl")
    private String summary;

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getSummary() { return summary; }

    public void setSummary(String summary) { this.summary = summary; }
}
