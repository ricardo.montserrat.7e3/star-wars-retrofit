package cat.itb.starwarsretrofit.WebService;

import cat.itb.starwarsretrofit.Objects.Data;
import cat.itb.starwarsretrofit.Objects.Film;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface WebServiceClient
{
    @GET()
    Call<Data> getPeople(@Url String url);

    @GET()
    Call<Film> getFilmData(@Url String url);
}