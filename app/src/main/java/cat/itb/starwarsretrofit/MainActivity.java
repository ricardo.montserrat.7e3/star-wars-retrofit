package cat.itb.starwarsretrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cat.itb.starwarsretrofit.Adapter.CharacterAdapter;
import cat.itb.starwarsretrofit.Objects.Character;
import cat.itb.starwarsretrofit.Objects.Data;
import cat.itb.starwarsretrofit.WebService.WebServiceClient;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener
{
    private CharacterAdapter characterAdapter;

    public static WebServiceClient webServiceClient;
    private Data currentData;

    private Button nextButton, goBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        characterAdapter = new CharacterAdapter(new ArrayList<>(), character ->
        {
            Intent intent = new Intent(MainActivity.this, CharacterFilms.class);
            intent.putStringArrayListExtra("films", new ArrayList<>(Arrays.asList(character.getFilms())));
            startActivity(intent);
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setAdapter(characterAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        nextButton = findViewById(R.id.next_button);
        goBackButton = findViewById(R.id.goback_button);

        nextButton.setOnClickListener(this);
        goBackButton.setOnClickListener(this);

        SearchView searchView = findViewById(R.id.searcher);
        searchView.setOnQueryTextListener(this);

        sendRequest();

        getPeopleData("people/", false);
    }

    private void sendRequest()
    {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/").addConverterFactory(GsonConverterFactory.create()).client(clientBuilder.build()).build();

        webServiceClient = retrofit.create(WebServiceClient.class);
    }

    private void getPeopleData(String url, boolean isSearch)
    {
        Call<Data> call = webServiceClient.getPeople(url);

        call.enqueue(new Callback<Data>()
        {
            @Override
            public void onResponse(@NotNull Call<Data> call, @NotNull Response<Data> response)
            {
                assert response.body() != null;
                if(!isSearch) currentData = response.body();
                characterAdapter.setCharacters(response.body().getResults());

                goBackButton.setEnabled(!currentData.getPrevious().isEmpty());
                nextButton.setEnabled(!currentData.getNext().isEmpty());
            }

            @Override
            public void onFailure(@NotNull Call<Data> call, @NotNull Throwable t)
            {
                System.out.println("Callback Failure: " + t.toString());
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.goback_button:
                getPeopleData(currentData.getPrevious(), false);
                break;
            case R.id.next_button:
                getPeopleData(currentData.getNext(), false);
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query)
    {
        if(!characterAdapter.filter(query)) getPeopleData("people/?search="+query.toLowerCase(), true);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) { return false; }
}