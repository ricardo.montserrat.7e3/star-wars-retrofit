package cat.itb.starwarsretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cat.itb.starwarsretrofit.Objects.Film;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cat.itb.starwarsretrofit.MainActivity.webServiceClient;

public class CharacterFilms extends AppCompatActivity implements View.OnClickListener
{
    private ArrayList<String> films;
    private int pointer = 0;

    private TextView title, summary;
    private Button nextButton, previousButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_films);

        Bundle values = getIntent().getExtras();
        if (values != null)
            films = values.getStringArrayList("films");

        title = findViewById(R.id.title_text_view);
        summary = findViewById(R.id.episode_summary_text_view);

        nextButton = findViewById(R.id.next_button_films);
        previousButton = findViewById(R.id.goback_button_films);

        nextButton.setEnabled(films.size() > 1);
        previousButton.setEnabled(films.size() > 1);

        nextButton.setOnClickListener(this);
        previousButton.setOnClickListener(this);

        loadFilmData(getFilmUrl(films.get(pointer)));
    }

    private void loadFilmData(String url)
    {
        Call<Film> call = webServiceClient.getFilmData(url);

        call.enqueue(new Callback<Film>()
        {
            @Override
            public void onResponse(@NotNull Call<Film> call, @NotNull Response<Film> response)
            {
                Film currentFilm = response.body();
                assert currentFilm != null;
                summary.setText(currentFilm.getSummary());
                title.setText(currentFilm.getTitle());
                updateButtonsState();
            }

            @Override
            public void onFailure(@NotNull Call<Film> call, @NotNull Throwable t)
            {
                System.out.println("Callback Failure: " + t.toString());
            }
        });
    }

    private void updateButtonsState()
    {
        if (films.size() < 2)
        {
            nextButton.setVisibility(View.INVISIBLE);
            previousButton.setVisibility(View.INVISIBLE);
            return;
        }
        nextButton.setEnabled(pointer < films.size() - 1);
        previousButton.setEnabled(pointer > 0);
    }

    private String getFilmUrl(String url)
    {
        String[] urlParts = url.split("/");
        return "films/" + urlParts[urlParts.length - 1] + "/";
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.goback_button_films:
                pointer--;
                loadFilmData(getFilmUrl(films.get(pointer)));
                break;
            case R.id.next_button_films:
                pointer++;
                loadFilmData(getFilmUrl(films.get(pointer)));
                break;
        }
    }
}